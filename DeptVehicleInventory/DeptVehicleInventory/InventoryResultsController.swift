//
//  InventoryResultsController.swift
//  DeptVehicleInventory
//
//  Created by Jason Loyd on 1/7/15.
//  Copyright (c) 2015 Jason Loyd. All rights reserved.
//

import UIKit

class IventoryResultsController: UIViewController, UITableViewDataSource, UITableViewDelegate, APIControllerProtocol {
    
    var vehicles = [Vehicle]()
    var api : APIController?
    var imageCache = [String: UIImage]()
    
    let kCellIdentifier: String = "SearchResultCell"
    
    @IBOutlet var appsTableView: UITableView?
    
    @IBAction func cancelToInventoryViewController(segue:UIStoryboardSegue) {
        println("trying to cancel...")
        dismissViewControllerAnimated(true, completion: nil)
        println("cancel")
    }
    
    @IBAction func saveInventoryDetail(segue:UIStoryboardSegue) {
        println("trying to be done")
        dismissViewControllerAnimated(true, completion: nil)
        println("done")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        api = APIController(delegate: self)
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        api!.getInventory()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        println("segue identifier")
        println(segue.identifier)
        if(segue.identifier == "vDetails") {
            var detailsViewController: DetailsViewController = segue.destinationViewController as DetailsViewController
            var vehicleIndex = appsTableView!.indexPathForSelectedRow()!.row
            var selectedVehicle = self.vehicles[vehicleIndex]
            detailsViewController.vehicle = selectedVehicle
        }
    }
    
    func didReceiveAPIResults(results: NSDictionary) {
        var resultsArr: NSArray = results["results"] as NSArray
        dispatch_async(dispatch_get_main_queue(), {
            self.vehicles = Vehicle.vehiclesWithJSON(resultsArr)
            self.appsTableView!.reloadData()
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        })
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section:    Int) -> Int {
        return vehicles.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier(kCellIdentifier) as UITableViewCell
        
        let vehicle = self.vehicles[indexPath.row]
        
        let cellText: String? = vehicle.vMake
        cell.textLabel?.text = cellText
        
        let deptCellText: Int = vehicle.deptNumber
        let detCellText: String = vehicle.vModel
        
        // TODO get thumbnails
        // use background thread to get the image for this item
        
        //let urlString = vehicle.thumbnailImageURL
        
        // Check our image cache for the existing key. This is just a dictionary of UIImages
        //var image: UIImage? = self.imageCache.valueForKey(urlString) as? UIImage
        //var image = self.imageCache[urlString]
        
        
//        if( image == nil ) {
//            // If the image does not exist, we need to download it
//            var imgURL: NSURL = NSURL(string: urlString)!
//            
//            // Download an NSData representation of the image at the URL
//            let request: NSURLRequest = NSURLRequest(URL: imgURL)
//            NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue(), completionHandler: {(response: NSURLResponse!,data: NSData!,error: NSError!) -> Void in
//                if error == nil {
//                    image = UIImage(data: data)
//                    
//                    // Store the image in to our cache
//                    self.imageCache[urlString] = image
//                    dispatch_async(dispatch_get_main_queue(), {
//                        if let cellToUpdate = tableView.cellForRowAtIndexPath(indexPath) {
//                            cellToUpdate.imageView?.image = image
//                        }
//                    })
//                }
//                else {
//                    println("Error: \(error.localizedDescription)")
//                }
//            })
//            
//        }
//        else {
//            dispatch_async(dispatch_get_main_queue(), {
//                if let cellToUpdate = tableView.cellForRowAtIndexPath(indexPath) {
//                    cellToUpdate.imageView?.image = image
//                }
//            })
//        }
        
        cell.detailTextLabel!.text = "\(detCellText), Department #: \(deptCellText)"
        
        return cell
    }
    
}

