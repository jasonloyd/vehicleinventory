//
//  DetailsViewController.swift
//  DeptVehicleInventory
//
//  Created by Jason Loyd on 1/8/15.
//  Copyright (c) 2015 Jason Loyd. All rights reserved.
//

import Foundation

import UIKit

class DetailsViewController: UIViewController {
    
    @IBOutlet weak var detailsLabel: UILabel!
    @IBOutlet weak var makeLabel: UILabel!
    @IBOutlet weak var modelLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var deptLabel: UILabel!
    @IBOutlet weak var mileageLabel: UILabel!
    @IBOutlet weak var availabilityLabel: UILabel!
    @IBOutlet weak var lastDrivenByLabel: UILabel!
    
    var vehicle: Vehicle?
    
    required init(coder aDecoder: (NSCoder!)) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        detailsLabel.text = "Vehicle Details:"
        makeLabel.text = self.vehicle?.vMake
        modelLabel.text = self.vehicle?.vModel
        yearLabel.text = "\(self.vehicle!.vYear)"
        deptLabel.text = "Department #: \(self.vehicle!.deptNumber)"
        mileageLabel.text = "Current Mileage: \(self.vehicle!.mileage)"
        if self.vehicle!.checkedIn {
            availabilityLabel.text = "Available: yes"
        } else {
            availabilityLabel.text = "Available: no"
        }
        // TODO format last driven date
        //let dateFormatter = NSDateFormatter()
        //dateFormatter.dateFormat = "MM-DD-YYYY"
        //let lastDrivenOn = dateFormatter.dateFromString(self.vehicle!.lastUpdated)
        lastDrivenByLabel.text = "Last driven by \(self.vehicle!.updatedBy)"
    }
}