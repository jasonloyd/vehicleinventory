//
//  Vehicle.swift
//  DeptVehicleInventory
//
//  Created by Jason Loyd on 1/7/15.
//  Copyright (c) 2015 Jason Loyd. All rights reserved.
//

//create_table "vehicles", force: :cascade do |t|
//    t.string   "vehicle_make"
//    t.string   "vehicle_model"
//    t.integer  "vehicle_year"
//    t.integer  "dept_number"
//    t.string   "updated_by"
//    t.datetime "last_updated"
//    t.integer  "mileage"
//    t.boolean  "checked_in"
//    t.datetime "created_at",    null: false
//    t.datetime "updated_at",    null: false
//end

import Foundation

class Vehicle {
    var vMake: String
    var vModel: String
    var vYear: Int
    var deptNumber: Int
    var updatedBy: String
    var lastUpdated: String
    var mileage: Int
    var checkedIn: Bool
    
    init(vMake: String, vModel: String, vYear: Int, deptNumber: Int, updatedBy: String, lastUpdated: String, mileage: Int, checkedIn: Bool) {
        self.vMake = vMake
        self.vModel = vModel
        self.updatedBy = updatedBy
        self.vYear = vYear
        self.deptNumber = deptNumber
        self.lastUpdated = lastUpdated
        self.mileage = mileage
        self.checkedIn = checkedIn
    }
    
    class func vehiclesWithJSON(allResults: NSArray) -> [Vehicle] {
        var vehicles = [Vehicle]()
        if allResults.count > 0 {
            for result in allResults {
                var _make = result["vehicle_make"] as? String ?? "Unknown"
                var _model = result["vehicle_model"] as? String ?? "Unknown"
                var _updatedBy = result["updated_by"] as? String ?? "Unknown"
                var _vYear = result["vehicle_year"] as? Int ?? 2015
                var _deptNumber = result["dept_number"] as? Int ?? 0
                var _lastUpdated = result["updated_at"] as? String ?? "Unknown"
                var _mileage = result["mileage"] as? Int ?? 0
                var _checkedIn = result["checked_in"] as? Bool ?? false
                
                var newVehicle = Vehicle(vMake: _make, vModel: _model, vYear: _vYear, deptNumber: _deptNumber, updatedBy: _updatedBy, lastUpdated: _lastUpdated, mileage: _mileage, checkedIn: _checkedIn)
                vehicles.append(newVehicle)
            }
        }
        return vehicles
    }

}